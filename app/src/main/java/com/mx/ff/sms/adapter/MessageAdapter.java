package com.mx.ff.sms.adapter;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.mx.ff.belilim_scanner_android.view.custom.MyIcon;
import com.mx.ff.sms.R;
import com.mx.ff.sms.core.model.Message;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.vH> {

    private Context context;
    private Activity activity;
    private ArrayList<Message> messageArrayList;
    private RecyclerView recycler;

    public MessageAdapter(Context context, RecyclerView recycler) {
        this.context = context;
        this.activity = (Activity) context;
        this.messageArrayList = new ArrayList<>();
        this.recycler = recycler;
    }

    public ArrayList<Message> getBlockList() {
        return messageArrayList;
    }

    @Override
    public vH onCreateViewHolder(ViewGroup parent, int viewType) {
        View vista = LayoutInflater.from(context)
                .inflate(R.layout.item_message, parent, false);

        vista.setOnClickListener(view -> {
            int position = recycler.getChildAdapterPosition(vista);
            /*Message acctualMessage = messageArrayList.get(position);
            ((SenderActivity) activity).sendSMS(acctualMessage);
            notifyDataSetChanged();*/
        });

        return new vH(vista);
    }

    @Override
    public void onBindViewHolder(vH holder, int position) {
        final Message messaje = messageArrayList.get(position);
        // holder.setDatos("Noticia", post.getCause(), "Requisitos para el Trámite de Modificación de la Información del Registro Federal de las OSC", post.getBody(),"Naucalpan de Juárez, Méx.","https://image.prntscr.com/image/VBZmqniaQTmPruJwB6HckA.png","");
        holder.setDatos(messaje);
    }

    @Override
    public int getItemCount() {
        return messageArrayList.size();
    }

    public void addMessages(@NonNull List<Message> listaRecibida) {
        this.messageArrayList.clear();
        if (listaRecibida.isEmpty()) {
            Toast.makeText(context, "Ningun resultado", Toast.LENGTH_SHORT).show();
        } else {
            this.messageArrayList.addAll(listaRecibida);
        }
        notifyDataSetChanged();
    }

    public void addMessage(Message message) {
        this.messageArrayList.add(message);
        notifyDataSetChanged();
    }

    public void removeMessage(Message comment, int position) {
        messageArrayList.remove(comment);
        notifyItemRemoved(position);

    }

    public Message removeMessage() {
        Message message=messageArrayList.remove(0);
        notifyItemRemoved(0);
        return message;
    }

    public void setMessage(Message message, int position) {
        notifyItemChanged(position, messageArrayList.get(position));
    }

    public ArrayList<Message> getMessageArrayList() {
        return messageArrayList;
    }

    class vH extends RecyclerView.ViewHolder {

        TextView messageTextView, phoneTextView, dateTextView;
        MyIcon checkIcon,doubleCheckIcon;

        vH(View vista) {
            super(vista);
            phoneTextView = vista.findViewById(R.id.textView);
            messageTextView = vista.findViewById(R.id.textView2);
            dateTextView = vista.findViewById(R.id.textView3);
            checkIcon=vista.findViewById(R.id.i_message_check_icon);
            doubleCheckIcon=vista.findViewById(R.id.i_message_double_check_icon);

            checkIcon.setVisibility(View.GONE);
            doubleCheckIcon.setVisibility(View.GONE);


        }


        void setDatos(Message message) {
            messageTextView.setText(String.format("%s", message.getMessage()));
            phoneTextView.setText(String.format("%s", message.getDn()));
            dateTextView.setText(String.format("%s", message.getReceivedDate()));
            checkIcon.setVisibility("Sin fecha".equals(message.getSendDate())?View.GONE:View.VISIBLE);
            doubleCheckIcon.setVisibility("Sin fecha".equals(message.getReceivedDate())?View.GONE:View.VISIBLE);
        }
    }
}
