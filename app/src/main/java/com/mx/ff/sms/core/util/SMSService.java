package com.mx.ff.sms.core.util;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.telephony.SmsMessage;
import android.util.Log;

import com.mx.ff.sms.core.ejb.AlertsEJB;

public class SMSService extends Service {

    private IntentFilter mIntentFilter;
    private SMSreceiver mSMSreceiver;
    private final String TAG = this.getClass().getSimpleName();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: SMS service");
        //SMS event receiver
        mSMSreceiver = new SMSreceiver();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(mSMSreceiver, mIntentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Unregister the SMS receiver
        unregisterReceiver(mSMSreceiver);
    }

    private String ConvertNumber(String from) {
        return from;
    }

    public class SMSreceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            String strMessage = "";

            Log.e(TAG, "Got a message!!");
            if (bundle != null) {
                try {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    SmsMessage[] msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        String msg_from = msgs[i].getOriginatingAddress();
                        String msgBody = msgs[i].getMessageBody();
                        Log.e(TAG, "Message body ::" + msgBody);
                        String [] id=msgBody.split(":");
                        Log.e(TAG, id[2] );
                        if (id[0].equals("PPE"))
                        new AlertsEJB(response -> {}).sendAlert(id[2]);
                    }
                } catch (Exception e) {
                    Log.d(TAG,e.getMessage());
                }

            }
        }
    }

}