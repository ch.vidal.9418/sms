package com.mx.ff.sms.core.model;

import com.mx.ff.sms.core.util.Response;
import com.mx.ff.sms.core.util.ResponseCode;

import org.json.JSONException;
import org.json.JSONObject;

public class TextMessage {

    public static String JSON_BODY = "texto";
    public static String JSON_PHONE = "telefono";
    public static String JSON_ID = "id";
    public static String JSON_DATE = "date";

    private String messages;
    private String number;
    private String fecha;
    private String status;
    private String id;

    public String getMessages() {
        return messages;
    }

    public TextMessage setMessages(String messages) {
        this.messages = messages;
        return this;
    }

    public String getNumber() {
        return number;
    }

    public TextMessage setNumber(String number) {
        this.number = number;
        return this;
    }

    public String getFecha() {
        return fecha;
    }

    public TextMessage setFecha(String fecha) {
        this.fecha = fecha;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public TextMessage setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getId() {
        return id;
    }

    public TextMessage setId(String id) {
        this.id = id;
        return this;
    }

    public static Response<TextMessage> fromJson(JSONObject jsonMember) {
        Response<TextMessage> response = new Response<>();
        try {
            TextMessage cm = new TextMessage();
            if(jsonMember.has(JSON_PHONE)) cm.number = jsonMember.getString(JSON_PHONE);
            if(jsonMember.has(JSON_BODY)) cm.messages = jsonMember.getString(JSON_BODY);
            if(jsonMember.has(JSON_DATE)) cm.fecha = jsonMember.getString(JSON_DATE);
            if(jsonMember.has(JSON_ID)) cm.id = jsonMember.getString(JSON_ID);


            response.setData(cm);
        } catch (JSONException e) {
            response.makeError(ResponseCode.JSON_ERROR,
                    e.getMessage(),
                    TextMessage.class.getSimpleName());
        } catch (NumberFormatException e) {
            response.setError(e.getLocalizedMessage());
        }

        return response;
    }
}
