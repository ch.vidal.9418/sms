package com.mx.ff.sms.core.net;

public class Routes {
    public static final String SERVER_URL                  = "***************************************";
    public static final String SQUEDULER_URL               = "***scheduler-284021.uc.r.appspot.com/api/";
    //public static final String SERVER_URL                    = "http://165.227.199.69:8083/api/";
     //public static final String SERVER_URL                 = "http://192.168.50.244:8091/api/";

    public static final String GET_METHOD               = "GET";
    public static final String PUT_METHOD               = "PUT";
    public static final String POST_METHOD              = "POST";
    public static final String PATCH_METHOD              = "PATCH";
    public static final String DELETE_METHOD            = "DELETE";

    // User
    public static final String GET_MESSAGES             = "messages";
    public static final String UPDATE_IS_SENT           = "updateIsSent";
    public static final String UPDATE_IS_DELIVERED      = "updateIsDelivered";

    // Sockets
    public static final String SOCKET_URL               = SERVER_URL + "espacio-del-chat";
    public static final String SEND_SMS                 = "enviaSMS";
    public static final String SEND_SMS_BLOCK           = "enviaSMSblock";
}
