package com.mx.ff.sms.core.ejb;

import android.app.Activity;

import com.mx.ff.sms.core.model.TextMessage;
import com.mx.ff.sms.core.net.Routes;
import com.mx.ff.sms.core.net.WebService;
import com.mx.ff.sms.core.util.AsyncResponse;
import com.mx.ff.sms.core.util.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.mx.ff.sms.core.Constantes.SENT_MESSAGES;
import static com.mx.ff.sms.core.net.Routes.UPDATE_IS_DELIVERED;
import static com.mx.ff.sms.core.net.Routes.UPDATE_IS_SENT;


public class MessagesEJB extends CustomEJB {


    private static final String TAG = MessagesEJB.class.getSimpleName();
    private Activity act;


    public MessagesEJB(AsyncResponse viewUpdater, Activity activity) {
        super(viewUpdater);
        this.act = activity;
    }

    public void getMessages(int option) {
        try {
            new WebService(this::searchCallback,
                    Routes.GET_METHOD,
                    Routes.GET_MESSAGES)
                    .execute(new JSONObject().put("option", option));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void updateMessages(String id, String option) {
        try {
            new WebService(this::searchCallback,
                    Routes.POST_METHOD,
                    SENT_MESSAGES.equals(option) ? UPDATE_IS_SENT : UPDATE_IS_DELIVERED)
                    .execute(new JSONObject().put("id", id));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void searchCallback(Response<JSONObject> response) {
        try {

            Response<ArrayList<TextMessage>> forward = new Response<>();
            JSONArray jsonArray = (JSONArray) (response.getData()).get("gatito");
            ArrayList<TextMessage> textMessageArrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
                textMessageArrayList.add(TextMessage.fromJson((JSONObject) jsonArray.get(i)).getData());
            forward.setData(textMessageArrayList);
            act.runOnUiThread(() -> {
                if (validateViewUpdater()) viewUpdater.processFinish(forward);
            });
        } catch (Exception e) {

        }
    }

    private void retriveBlocks(){
        try {
            new WebService(this::searchCallback,
                    Routes.GET_METHOD,
                    "campaigns")
                    .execute(new JSONObject().put("platformId", 1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void retriveBlocksCallbck(Response<JSONObject> response) {
        try {
            Response<ArrayList<TextMessage>> forward = new Response<>();
            JSONArray jsonArray = (JSONArray) (response.getData()).get("gatito");
            ArrayList<TextMessage> textMessageArrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++)
                textMessageArrayList.add(TextMessage.fromJson((JSONObject) jsonArray.get(i)).getData());
            forward.setData(textMessageArrayList);
            act.runOnUiThread(() -> {
                if (validateViewUpdater()) viewUpdater.processFinish(forward);
            });
        } catch (Exception e) {

        }
    }



}
