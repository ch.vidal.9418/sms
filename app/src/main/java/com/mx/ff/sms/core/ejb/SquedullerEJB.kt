package com.mx.ff.sms.core.ejb

import android.util.Log
import com.mx.ff.sms.core.net.Routes.POST_METHOD
import com.mx.ff.sms.core.net.Routes.SQUEDULER_URL
import com.mx.ff.sms.core.net.WebService
import com.mx.ff.sms.core.util.AsyncResponse
import com.mx.ff.sms.core.util.Response
import com.mx.ff.sms.core.util.ResponseCode
import com.mx.ff.sms.core.util.Sesion
import org.json.JSONObject

class SquedullerEJB(viewUpdater: AsyncResponse) : CustomEJB(viewUpdater) {
    private val TAG = CustomEJB::class.java.simpleName
    private val BASE_URI = "users/"

    fun getToken(userName:String,pass:String) {

        val data = JSONObject()
        data.put("username", userName)
        data.put("password", pass)

        WebService(
                getTokenCallback,
                POST_METHOD,
                BASE_URI +"login",
                SQUEDULER_URL
        ).execute(data)
    }

    private val getTokenCallback = AsyncResponse { response: Response<*> ->
        val forward: Response<String> = response.convert()
        val res = ""
        forward.data = res
        try {
            if (response.isOk) {
                val userJson = JSONObject(response.data.toString())
                Sesion.getSesion().token = userJson.getString("token")
            } else response.makeError()
        } catch (e: Exception) {
            response.makeError(ResponseCode.UNKNOWN_ERROR, e.message, TAG)
            e.printStackTrace()
            Log.e(TAG, response.toString())
        }
        if (validateViewUpdater()) viewUpdater.processFinish(forward)
    }

    fun postFirebaseTokenId(token: String) {

        val data = JSONObject()

        if (!token.isBlank())
            data.put("token", token)

        WebService(
                basicCallbck,
                POST_METHOD,
                "connections",
                SQUEDULER_URL
        ).execute(data)
    }

    val basicCallbck = AsyncResponse { response: Response<*> ->
        if (!response.isOk) response.makeError()
        if (validateViewUpdater()) viewUpdater.processFinish(response)
    }

    fun createUsername(userName:String) {

        val data = JSONObject()
        data.put("username", userName)


        WebService(
                createUsernameCallback,
                POST_METHOD,
                BASE_URI,
                SQUEDULER_URL
        ).execute(data)
    }

    private val createUsernameCallback = AsyncResponse { response: Response<*> ->
        val forward: Response<String> = response.convert()
        val res = ""
        forward.data = res
        try {
            if (response.isOk) {
                val userJson = JSONObject(response.data.toString())
                Sesion.getSesion().token = userJson.getString("token")
            } else response.makeError()
        } catch (e: Exception) {
            response.makeError(ResponseCode.UNKNOWN_ERROR, e.message, TAG)
            e.printStackTrace()
            Log.e(TAG, response.toString())
        }
        if (validateViewUpdater()) viewUpdater.processFinish(forward)
    }

}