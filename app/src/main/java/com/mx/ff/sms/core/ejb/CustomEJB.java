package com.mx.ff.sms.core.ejb;


import com.mx.ff.sms.core.util.AsyncResponse;
import com.mx.ff.sms.core.util.Response;

import org.json.JSONObject;

/**
 * Provides information to view
 */

public abstract class CustomEJB {
    protected AsyncResponse viewUpdater;

    public CustomEJB() {
        viewUpdater = null;
    }

    protected CustomEJB(AsyncResponse viewUpdater) {
        this.viewUpdater = viewUpdater;
    }

    public AsyncResponse getViewUpdater() {
        return viewUpdater;
    }

    public void setViewUpdater(AsyncResponse viewUpdater) {
        this.viewUpdater = viewUpdater;
    }

    protected boolean validateViewUpdater() {
        return viewUpdater != null;
    }

    protected void basicCallback(Response<JSONObject> response) {
        if (!response.isOk()) response.makeError();
        if (validateViewUpdater()) viewUpdater.processFinish(response);
    }
}
