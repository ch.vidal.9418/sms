package com.mx.ff.sms.core.model

import android.os.Parcel
import android.os.Parcelable

data class RemoteMessagge(
    val dn: Long,
    val message: String,
    val transactionId: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(dn)
        parcel.writeString(message)
        parcel.writeString(transactionId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RemoteMessagge> {
        override fun createFromParcel(parcel: Parcel): RemoteMessagge {
            return RemoteMessagge(parcel)
        }

        override fun newArray(size: Int): Array<RemoteMessagge?> {
            return arrayOfNulls(size)
        }
    }
}