package com.mx.ff.sms.core.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.mx.ff.sms.core.model.TextMessage;

import java.util.ArrayList;

/**
 * Persistence end-point
 */

public class Sesion {


    private final static String TAG = Sesion.class.getSimpleName();
    private static final Sesion instance = new Sesion();
    private final static String TOKEN_PREF = "token";
    private final static String USERNAME_PREF = "username";
    private final static String PASSWORD_PREF = "password";
    private final static String LOCK_PREF = "lock";
    private String tmpType = null;
    private SharedPreferences prefs;
    private ArrayList<TextMessage> causes;
    private String messagesJson;
    private Sesion() {
        prefs = null;
    }

    public static Sesion getSesion() {
        return instance;
    }

    public void init(Context ctx) {
        prefs = ctx.getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        causes=new ArrayList<>();
    }

    public String getToken() {
        return prefs.getString(TOKEN_PREF, "");
    }

    public void setToken(String token) {
        prefs.edit().putString(TOKEN_PREF, token).commit();
    }

     public String getUserName() {
        return prefs.getString(USERNAME_PREF, "");
    }

    public void setUserName(String token) {
        prefs.edit().putString(USERNAME_PREF, token).commit();
    }

    public Boolean isAvalaible() {
        return prefs.getBoolean(LOCK_PREF, true);
    }

    public void setAvalaible(boolean token) {
        prefs.edit().putBoolean(LOCK_PREF, token).commit();
    }

     public String getPassword() {
        return prefs.getString(PASSWORD_PREF, "l0nx4vc78m01fjx493i7f5dpkm1234xspo23kdmvt36905ns56d8frim49ljb06rxd");
    }

    public void setPassword(String token) {
        prefs.edit().putString(PASSWORD_PREF, token).commit();
    }

    public ArrayList<TextMessage> getCauses() {
        return causes;
    }

    public void addCause(){

    }

    public void setCauses(ArrayList<TextMessage> causes) {
        this.causes = causes;
    }


}
