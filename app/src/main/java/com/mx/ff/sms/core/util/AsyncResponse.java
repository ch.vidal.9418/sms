package com.mx.ff.sms.core.util;

/**
 * Esta clase se encarga de procesar las respuestas del WebService
 */
public interface AsyncResponse {
    /**
     * Método de callback
     */
    void processFinish(Response response);
}
