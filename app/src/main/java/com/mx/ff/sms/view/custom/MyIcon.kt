package com.mx.ff.belilim_scanner_android.view.custom

import android.content.Context
import android.graphics.PorterDuff
import android.util.AttributeSet
import android.widget.ImageView
import com.mx.ff.sms.R

class MyIcon : ImageView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }

    private fun init(set: AttributeSet?) {
        val a = context.obtainStyledAttributes(set, R.styleable.ColorOptionsView)
        val valueColor = a.getColor(R.styleable.ColorOptionsView_valueColor, resources.getColor(R.color.colorAccent))
        drawable.mutate().setColorFilter(valueColor, PorterDuff.Mode.SRC_ATOP)
        a.recycle()
    }
}
