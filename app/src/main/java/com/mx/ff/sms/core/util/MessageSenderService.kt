package com.mx.ff.sms.core.util

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.telephony.SmsManager
import android.widget.Toast
import com.mx.ff.sms.R
import com.mx.ff.sms.core.ejb.CampaignsEJB
import com.mx.ff.sms.core.ejb.UserEJB
import com.mx.ff.sms.core.model.Message
import com.mx.ff.sms.core.model.RemoteMessagge
import com.mx.ff.sms.view.activity.LoginActivity

class MessageSenderService : Service() {
    private val CHANNEL_ID = "Message Service"
    private var total = 0
    private var sended = 0
    private var received = 0
    private var campaign = "Mensaje Unitario"
    private var status = ""


    companion object {
        fun startService(context: Context, messagge: RemoteMessagge) {
            val startIntent = Intent(context, MessageSenderService::class.java)
            startIntent.putExtra("message", messagge)
            ContextCompat.startForegroundService(context, startIntent)
        }

        fun stopService(context: Context) {
            val stopIntent = Intent(context, MessageSenderService::class.java)
            context.stopService(stopIntent)
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        status="Iniciando"
        createNotificationChannel()
        refreshNotification()
        Sesion.getSesion().init(this)
        UserEJB(AsyncResponse {
            if (it.isOk) {
                status="Enviado"
                val remoteMessagge=intent?.extras?.get("message") as RemoteMessagge
                sendSMS(remoteMessagge)
                Toast.makeText(this, "Sending", Toast.LENGTH_SHORT).show()
            } else Toast.makeText(this, "Ups! Something was wrong", Toast.LENGTH_SHORT).show()
        }).getToken(Sesion.getSesion().userName,Sesion.getSesion().password)
        //stopSelf();
        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Foreground Message Channel",
                    NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    fun sendSMS(message: RemoteMessagge) {

        val SENT = "SMS_SENT_" + message.transactionId
        val DELIVERED = "SMS_DELIVERED_" + message.transactionId

        val sentPI = PendingIntent.getBroadcast(this, 0,
                Intent(SENT), 0)

        val deliveredPI = PendingIntent.getBroadcast(this, 0,
                Intent(DELIVERED), 0)

        //---when the SMS has been sent---
        registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(arg0: Context, arg1: Intent) {
                var status = ""
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        status = "Enviado"
                        sended++

                    }
                    SmsManager.RESULT_ERROR_GENERIC_FAILURE -> status = "Generic Failure"
                    SmsManager.RESULT_ERROR_NO_SERVICE -> status = "No Service"
                    SmsManager.RESULT_ERROR_NULL_PDU -> status = "Null PDU"
                    SmsManager.RESULT_ERROR_RADIO_OFF -> status = "Radio off"
                }
                CampaignsEJB(AsyncResponse {}).setMessageStatus(message.transactionId, "SENDED")
                refreshNotification()
            }
        }, IntentFilter(SENT))

        //---when the SMS has been delivered---
        registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(arg0: Context, arg1: Intent) {
                var status = ""
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        status = "Entregado"
                        received++

                    }
                    Activity.RESULT_CANCELED -> status = "No Entregado"
                }
                refreshNotification()
                CampaignsEJB(AsyncResponse {}).setMessageStatus(message.transactionId, "RECEIVED")
            }
        }, IntentFilter(DELIVERED))

        val sms = SmsManager.getDefault()

        sms.sendTextMessage(message.dn.toString(), null, message.message, sentPI, deliveredPI)
    }

    fun refreshNotification() {
        val pendingIntent = PendingIntent.getActivity(this, 0, Intent(this, LoginActivity::class.java), 0)
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("SMS Masivos: $campaign")
                .setContentText("Estado:$status")
                .setSmallIcon(R.drawable.check_icon)
                .setContentIntent(pendingIntent)
                .build()
        startForeground(1, notification)
    }






}