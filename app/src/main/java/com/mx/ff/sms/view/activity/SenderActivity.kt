package com.mx.ff.sms.view.activity

import android.Manifest
import android.app.Activity
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.telephony.SmsManager
import android.view.View
import android.widget.Toast
import com.mx.ff.sms.adapter.MessageAdapter
import com.mx.ff.sms.R
import com.mx.ff.sms.core.ejb.CampaignsEJB
import com.mx.ff.sms.core.model.Message
import com.mx.ff.sms.core.util.AsyncResponse
import kotlinx.android.synthetic.main.activity_sender.*
import java.util.*

class SenderActivity : AppCompatActivity() {


    private lateinit var messagesList: List<Message>
    private lateinit var id: String
    private lateinit var messageAdapter: MessageAdapter
    private var campaignState = false
    private var total = 0
    private var received = 0
    private var sent = 0
    private var remaining = 0
    private var SEND_SMS_PERMISSIONS = 123


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sender)
        id = intent.extras.getString("campaignCreationDate")
        val recycler = a_blocks_recycler_view
        messageAdapter = MessageAdapter(this, recycler)
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler.adapter = messageAdapter
        requestSendSmsPermissions()
        retrieveMessageList()
        a_sender_swipe_refresh.setOnRefreshListener {
            retrieveMessageList()

        }
    }

    fun sendSMS(message: Message) {

        val SENT = "SMS_SENT_" + message.publicId
        val DELIVERED = "SMS_DELIVERED_" + message.publicId

        val sentPI = PendingIntent.getBroadcast(this, 0,
                Intent(SENT), 0)

        val deliveredPI = PendingIntent.getBroadcast(this, 0,
                Intent(DELIVERED), 0)

        //---when the SMS has been sent---
        registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(arg0: Context, arg1: Intent) {
                var status = ""
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        status = "Enviado"
                        sent++
                        remaining--
                        Thread {
                            runOnUiThread {
                                refreshCounter()
                            }
                        }.start()

                    }
                    SmsManager.RESULT_ERROR_GENERIC_FAILURE -> status = "Generic Failure"
                    SmsManager.RESULT_ERROR_NO_SERVICE -> status = "No Service"
                    SmsManager.RESULT_ERROR_NULL_PDU -> status = "Null PDU"
                    SmsManager.RESULT_ERROR_RADIO_OFF -> status = "Radio off"
                }
                messageAdapter.setMessage(message, messageAdapter.messageArrayList.indexOf(message))
                CampaignsEJB(AsyncResponse {}).setMessageStatus(message.publicId, "SENDED")
            }
        }, IntentFilter(SENT))

        //---when the SMS has been delivered---
        registerReceiver(object : BroadcastReceiver() {
            override fun onReceive(arg0: Context, arg1: Intent) {
                var status = ""
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        status = "Entregado"
                        received++
                        Thread {
                            runOnUiThread {
                                refreshCounter()
                            }
                        }.start()

                    }
                    Activity.RESULT_CANCELED -> status = "No Entregado"
                }

                CampaignsEJB(AsyncResponse {}).setMessageStatus(message.publicId, "RECEIVED")
            }
        }, IntentFilter(DELIVERED))

        val sms = SmsManager.getDefault()
        val partes = ArrayList<String>()

        sms.sendTextMessage(message.dn.toString(), null, message.message, sentPI, deliveredPI)
    }

    fun retrieveMessageList() {
        a_login_progress_bar.visibility = View.VISIBLE
        if (id.isNotEmpty())
            CampaignsEJB(AsyncResponse {
                a_login_progress_bar.visibility = View.INVISIBLE
                if (it.isOk) {
                    val data = it.data as List<Message>
                    total = data.size
                    a_sender_counter.text = "$total  Restantes"
                    messageAdapter.addMessages(data.reversed())
                    //sendCampaign()
                }
            }).retriveMessagesBlocksDetail(id)
        else Toast.makeText(this, "Id Malformado", Toast.LENGTH_SHORT).show();
    }

    fun refreshCounter() {
        a_sender_sends_count.text = "Enviados $sent"
        a_sender_receiveds_count.text = "Recibidos $received"
        a_sender_counter.text = "$received Restantes"
        if (sent == total) {
            a_sender_counter.text = "Finalizado :)"
            Toast.makeText(this, "Finalizado", Toast.LENGTH_SHORT).show();
        }

    }

    fun changeCampaignState(view: View) {

        campaignState = !campaignState
        a_sender_state_icon.setImageDrawable(resources.getDrawable(if (campaignState) R.drawable.play_icon else R.drawable.pause_icon))
        a_sender_state_icon.drawable.mutate().setColorFilter(resources.getColor(R.color.colorAccent), PorterDuff.Mode.SRC_ATOP)
        //sendCampaign()
    }

    fun sendCampaign() {

        messageAdapter.messageArrayList.forEach {
            Handler().postDelayed({
                sendSMS(it)
            }, 3000)
        }
    }

    fun requestSendSmsPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.SEND_SMS)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Permisos Malformados", Toast.LENGTH_SHORT).show()
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.SEND_SMS),
                    SEND_SMS_PERMISSIONS)
        }
    }

}
