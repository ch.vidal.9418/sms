package com.mx.ff.sms.core;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class SocketService extends Service {


    private String TAG = "SocketService";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate: SocketService iniciado");
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "SocketService destruido");
        super.onDestroy();
    }
}
