package com.mx.ff.sms.core.model.alerts

data class Coordinates(
    val lat: Double,
    val lon: Double
)