package com.mx.ff.sms.core;

import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;



public class Conector {

    private static Conector conector = new Conector();
    private Socket socket;
    private String TAG = "Conector";
    private Context ctx;

    private Conector() {
        try {
            //socket = IO.socket(Constantes.SERVER+Constantes.PUERTO);
            socket = IO.socket(Constantes.SERVER_DUMMY);
            Log.e("Socket", "Creado en " + Constantes.SERVER_DUMMY);
        } catch (URISyntaxException e) {
            Log.e("Socket", e.getMessage());
        }

    }

    public static Conector getInstance() {
        return conector;
    }

    public void conectar() {

        socket.connect();
        Log.e("Socket", "conectar: " + socket.connected());
        Log.e("Socket", "id: " + socket.id());

        /*On's a dar de alta*/
        recibidor();
        conectarUsuario();
        enviaMensaje();
    }

    /*************On***********/

    public void recibidor() {
        socket.on("recibeAlgo", new Emitter.Listener() {
            @Override
            public void call(Object... args) {

                Log.e("Socket", "intenando enviar mensaje ");
                //Log.e(TAG, "call: Hola " + socket.id() + "-" + Repartidor.getInstance().getId());
            }
        });
    }

    public void enviaMensaje() {
        socket.on("enviaSMS", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject data = (JSONObject) args[0];
                            String message = "";
                            String number = "";
                            Log.e( "Enviando...",data.toString());
                            message = data.getString("texto");
                            number = data.getString("telefono");
                            Log.d("Sockets", number + ":" + message);
                            Thread.sleep(8000);
                            SmsManager smsManager = SmsManager.getDefault();
                            smsManager.sendTextMessage(number, null, message, null, null);

                        } catch (InterruptedException | JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                Log.e("Socket", "intenando enviar mensaje ");


                //Log.e(TAG, "call: Hola " + socket.id() + "-" + Repartidor.getInstance().getId());
            }
        });
    }

    /*******emit*********/

    public void conectarUsuario() {
        ArrayList<String[]> a = new ArrayList<>();
        String b[] = {"idUsuario", "25"};
        a.add(b);
        try {
            socket.emit("conectarUsuario", Utilidades.generaJSON(a));
            Log.e("Sockets", "conectarUsuario" + Utilidades.generaJSON(a).toString());
        } catch (JSONException e) {
            Log.e("Sockets", e.toString());
            e.printStackTrace();
        }
    }

    public void aceptarGuia() {
    }

    public void declinarGuia() {
    }

    public Socket getSocket() {
        return socket;
    }

    public Context getCtx() {
        return ctx;
    }

    public void setCtx(Context ctx) {
        this.ctx = ctx;
    }
}
