package com.mx.ff.sms.core.util

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.mx.ff.sms.core.model.Campaign
import com.mx.ff.sms.core.model.RemoteMessagge
import org.json.JSONObject


class MyService : FirebaseMessagingService() {

    val TAG = "Firabase Sevice"

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        Log.e(TAG, remoteMessage?.data.toString())
        val aux = remoteMessage?.data
        val jsonObject = JSONObject(remoteMessage?.data)

        if (jsonObject.has("dn")) {
            val remoteMessage = Gson().fromJson(jsonObject.toString(), RemoteMessagge::class.java)
            MessageSenderService.startService(this, remoteMessage)
        } else if(jsonObject.has("isToSend")){
            val campaing = Gson().fromJson(jsonObject.toString(), Campaign::class.java)
            CampaignManagerService.startService(this, campaing.seconds)
            //Desde aqui se registra
        }
        /*
        val message = remoteMessage?.data?.get("message").toString()
        val event = remoteMessage?.data?.get("event").toString()

        val intent = Intent(this, MyService::class.java)
        intent.putExtra("message", message)
        intent.putExtra("event", event)

        showNotification(event, message, this, intent)
        ForegroundService.startService(this,seconds.getString("seconds"))*/

        /*startActivity(Intent(this, CallActivity::class.java))
        showNotification("El nodo quiere comunicarse contigo", "Cuerpo del mensaje", this)*/
    }

    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: $token")
    }

}
