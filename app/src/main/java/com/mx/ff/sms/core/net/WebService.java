package com.mx.ff.sms.core.net;

import android.icu.util.ULocale;
import android.os.AsyncTask;
import android.util.Log;

import com.mx.ff.sms.core.util.AsyncResponse;
import com.mx.ff.sms.core.util.Response;
import com.mx.ff.sms.core.util.ResponseCode;
import com.mx.ff.sms.core.util.Sesion;

import org.json.JSONObject;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Fetch information from server
 */

public class WebService extends AsyncTask<JSONObject, Integer, Response<JSONObject>> {
    private static final String TAG = WebService.class.getSimpleName();
    private static final MediaType JSON = MediaType.parse("application/json");

    private String requestMethod;
    private String requestRoute;
    private String URL;
    private AsyncResponse callback;

    private boolean uploading;
    private ArrayList<String> uploadingFiles;
    private String uploadingJsonProperty;

    /**
     * Query an empty route over GET without callback
     */
    public WebService() {
        this.callback = null;
        this.requestMethod = "GET";
        this.requestRoute = "/";
        this.uploading = false;
        this.uploadingFiles = new ArrayList<>();
    }

    /**
     * Query an specific route over an specific method with an specific callback
     *
     * @param callback      method executed before calling the server
     * @param requestMethod request method
     * @param requestRoute  route to fetch data
     */
    public WebService(AsyncResponse callback, String requestMethod, String requestRoute) {
        this.callback = callback;
        this.requestMethod = requestMethod;
        this.requestRoute = requestRoute;
        this.uploading = false;
        this.uploadingFiles = new ArrayList<>();
    }

    public WebService(AsyncResponse callback, String requestMethod, String requestRoute,String URL) {
        this.callback = callback;
        this.requestMethod = requestMethod;
        this.requestRoute = requestRoute;
        this.uploading = false;
        this.uploadingFiles = new ArrayList<>();
        this.URL= URL;
    }

    public WebService setUploadingFiles(ArrayList<String> uploadingFiles, String uploadingJsonProperty) {
        this.uploadingFiles = uploadingFiles;
        this.uploading = uploadingFiles.size() > 0;
        this.uploadingJsonProperty = uploadingJsonProperty;
        return this;
    }

    /**
     * Query the server in background
     *
     * @param params 0: Json object sent to server
     * @return JsonObject response
     */
    @Override
    protected Response<JSONObject> doInBackground(JSONObject... params) {
        Response<JSONObject> response = new Response<>();
        try {
            //Log.d(TAG, "req:"+ params[0].toString());
            OkHttpClient client = new OkHttpClient();
            URL url;
            if (this.URL==null)
            url = new URL(Routes.SERVER_URL + this.requestRoute);
            else  url = new URL(this.URL+ this.requestRoute);


            RequestBody body = RequestBody.create(JSON, params[0].toString());
            Request.Builder request = new Request.Builder()
                    .addHeader("authorization", "Bearer "+ Sesion.getSesion().getToken());
            Log.d(TAG + "-Request-" + requestRoute, params[0].toString());

            if (Routes.GET_METHOD.equals(this.requestMethod)) {
                String x = "?";
                Iterator<String> i = params[0].keys();
                while (i.hasNext()) {
                    String k = i.next();
                    x += k + "=" + params[0].get(k) + "&";
                }
                if (x.endsWith("&") || x.endsWith("?")) x = x.substring(0, x.length() - 1);

                request.url(url + x);
            } else {
                request.url(url)
                        .method(this.requestMethod, body);
            }

            if (uploading) {
                MultipartBody.Builder mpbuilder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM);
                for (String s : uploadingFiles) {
                    File f = new File(s);
                    mpbuilder.addFormDataPart(uploadingJsonProperty, f.getName(),
                            RequestBody.create(MediaType.parse("image/jpeg"), f));
                }
                Iterator<String> i = params[0].keys();
                while (i.hasNext()) {
                    String k = i.next();
                    mpbuilder.addFormDataPart(k, params[0].getString(k));
                }
                body = mpbuilder.build();
                request = request.method(this.requestMethod, body);
            }

            okhttp3.Response serverResponse = client.newCall(request.build()).execute();
            String s = serverResponse.body().string();
            Log.i(TAG + "-Response", "\n*******" + requestRoute + "*******\n" + s);
            response.setMessage(serverResponse.message());


            if (serverResponse.code() == 500 || serverResponse.code() == 401) {
                JSONObject res = new JSONObject(s);
                response.makeError(serverResponse.code(), res.getString("error"), res.getString("message"));
            } else if (serverResponse.code() == 204) response.setData(new JSONObject());
            else if (!s.contains("</html>")) response.setData(new JSONObject(s));
            else response.makeError(404, s, s);


        } catch (Exception e) {
            response.makeError(ResponseCode.UNKNOWN_ERROR,
                    e.getMessage(),
                    "Error al conectarse con el servidor");
            Log.e(TAG, e.getLocalizedMessage());
        }

        return response;
    }

    @Override
    protected void onPostExecute(Response<JSONObject> response) {
        super.onPostExecute(response);
        if (callback != null)
            callback.processFinish(response);
    }


}
