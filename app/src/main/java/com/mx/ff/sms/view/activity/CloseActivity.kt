package com.mx.ff.sms.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.se.omapi.Session
import com.mx.ff.sms.R
import com.mx.ff.sms.core.util.CampaignSenderService
import com.mx.ff.sms.core.util.Sesion

class CloseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_close)
        Sesion.getSesion().init(this)
        Sesion.getSesion().isAvalaible=false
        CampaignSenderService.stopService(this)
    }
}
